import java.util.*;
import java.io.*;

// ぞれぞれの文章を表示するにあたって、時間を少し置きたい　又は、一行ずつ表示
// でも毎回エンターキー押すようなコード書くと長くなる？
public class Main {
    public static void main(String[] args) {
    // キャラクターの生成
    Nobunaga n = new Nobunaga();
    n.name = "織田信長";
    Hideyoshi h = new Hideyoshi();
    h.name = "羽柴秀吉";
    Akechi a = new Akechi();
    a.name = "明智光秀";
    Player p = new Player();

    // Scene 01 Cut 01
    // ナレーション   <ppt 1>
    try{
    System.out.println("ある日、あなたは歴史の授業中に居眠りをしていました。");
    System.out.println("そして、目が覚めると、あなたは先程の教室でなく、あるお寺にいることに気づきました。");
    Thread.sleep(1000);  // ミリ秒単位
    System.out.println("あなたはそのことを一生懸命理解しようと思い、通り過ぎた坊主に声をかけてみました。");
    System.out.println("坊主はその日付とお寺の名前を教えてくれました。");
    Thread.sleep(1000);  // ミリ秒単位
    System.out.println("時は天正10年5月31日です。");
    System.out.println("そして、そのお寺の名前はあの有名な「本能寺」です。");
    Scanner scan = new Scanner(System.in);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    Thread.sleep(1000);  // ミリ秒単位
    // 名前を入力してもらう機能  <ppt 2>
    System.out.println("あなたの名前を教えてください");
    String playerName = new java.util.Scanner(System.in).nextLine();
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // ナレーション  <ppt 3>
    System.out.println("神：ようこそ" +playerName+ "さん、私はこのお寺を守っている神様です。");
    System.out.println("あなたがここにいる理由は、私が呼び出したからです。");
    System.out.println("");
    Thread.sleep(1000);  // ミリ秒単位
    System.out.println("今日は本能寺の変の前日です。");
    System.out.println("");
    System.out.println("そして、あなたは今、歴史を変えられる立場にいます。");
    System.out.println("つまり、本能寺の変が起こるか起こらないかは全部あなた次第です。");
    System.out.println("ただ、いや、やっぱりここまでにしましょう・・・");
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    Thread.sleep(1000);  // ミリ秒単位
    // ナレーション  <ppt 4>
    System.out.println("ふっと急に眩しい光があたりを包み、\n \n神様はその光の中で、姿が消えました。");
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // ----------------------------------------------------------------------------------------------------------------------------
// Scene 1 Cut 2
    // ナレーション <ppt 5-6>
    System.out.println("神様と別れた後、あなたはお寺でブラブラしている。");
    Thread.sleep(1000);  // ミリ秒単位
    System.out.println("そのとき、森の影から1人の男性があなたを見つめている。");
    Thread.sleep(1000);  // ミリ秒単位
    System.out.println("次の瞬間に、その謎の男は幽霊のように消えました。\n \n彼は誰だろう？\n \n何の企みなのか？\n \nあなたは大丈夫なのか？");
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // 本能寺の広さに驚いた  <ppt 7>
    p.speak41(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // Playerを取り調べるセリフ1  <ppt 8>
    n.question11(args);
    System.out.println("Press ss Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // ナレーション    <ppt 9>
    System.out.println("そう、先程の謎の男の正体は「蘭丸」と呼ばれる織田の家臣だ。\n \n彼はあなたの目撃情報をただちに信長に通報した。");
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // 信長との挨拶1  <ppt 10>
    p.question41(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // Playerを取り調べるセリフ2  <ppt 11>
    n.question12(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // 信長との挨拶2  <ppt 12>
    p.question42(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // Playerを取り調べるセリフ3  <ppt 13>
    n.question13(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // 自己紹介セリフ1  <ppt 14>
    p.speak42(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // Playerを取り調べるセリフ4  <ppt 15>
    n.question14(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // 自己紹介セリフ2  <ppt 16>
    p.speak43(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // ナレーション  <ppt 17>
    System.out.println("信長は刀であなたに向けて、\nしげしげと見ている。");
    Thread.sleep(1000);  // ミリ秒単位
    System.out.println("そして何らかを納得したように、\n信長は急に刀を収めた。");
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // Playerを取り調べるセリフ5  <ppt 18>
    n.question15(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // 信長への返事  <ppt 19>
    p.answer41(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // 場所用意を命令する  <ppt 20>
    n.speak11(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // 信長への返事  <ppt 21>
    p.answer41(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // ------------------------------------------------------------------------------------------
 // Scene 2
    // ナレーション <ppt 23>
    System.out.println("午後、信長は家臣たちを本能寺の本殿に集めた。");
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // 信長の天下構想を語る1  <ppt 24-25>
    n.speak12(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // ナレーション <ppt 26>
    System.out.println("その話を聞いて、家臣はものすごく興奮した。");
    Thread.sleep(1000);  // ミリ秒単位
    System.out.println("しかし、その中に一人がやや違うテンションで、ちょっと険しい目で信長を見ていた。\nただし、それは1秒を過ぎないことだった。");
    Thread.sleep(1000);  // ミリ秒単位
    System.out.println("その人は、実に本能寺の変の主犯とも言われている秀吉だ。");
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // 秀吉との対話1  <ppt 27>
    n.question16(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // 秀吉の天下構想を語る1  <ppt 28>
    h.speak21(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // 信長の天下構想を語る2  <ppt 29>
    n.speak13(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // 秀吉の天下構想を語る2  <ppt 30>
    h.speak22(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
// 秀吉との対話2  <ppt 31>
    n.speak14(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // ----------------------------------------------------------------------------------------------------------------------------
    //Scene 3 Cut 1
    //ナレーション <ppt 33>
    System.out.println("信長はあなたを呼び出した。");
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // Playerに本能寺の変を聞く  <ppt 34>
    n.question17(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    Thread.sleep(2000);  // ミリ秒単位
    // ナレーション <ppt 35>
    System.out.println("急に時間が止まった。あなたの目の前に神様がまた現れた。");;
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // ナレーション <ppt 36>
    System.out.println("神 : あなたが理想とする天下はどちらですか？");
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    //神様の選択へ返事する1  <ppt 37>
    p.answer42(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // ナレーション <ppt 38>
    System.out.println("神 : もし信長が描く天下を支持するなら信長に明智光秀の謀反を伝えましょう");
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // 神様の選択へ返事する2  <ppt 39>
    p.answer43(args);
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // ナレーション <ppt 40>
    System.out.println("神 : ただし、そうすると、あなたは永遠に消えてしまいます。\n \nもちろん、伝えない選択肢もありますよ！");
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // ナレーション <ppt 41>
    System.out.println("さて、どの未来を選びましょうか？");
    System.out.println("Press Enter Key...");
    scan.nextLine();
    System.out.println("\n ------------------------------------------------------------- \n");
    // 運命のEndingを選択する  <ppt 42>
    System.out.println("信長に本能寺の変を伝えますか？\n-------------------------------------------------\n1.伝える\n2.伝えない\n（数字のみ）");
        int choice = new java.util.Scanner(System.in).nextInt();
    Thread.sleep(2000);
    // Normal Hitsory
        if(choice == 1){
    // ナレーション <ppt 43>
            System.out.println("神様：そうですか。この未来を選びましたね！\n \nなら、気を付けてください。");
            System.out.println("Press Enter Key...");
            scan.nextLine();
            System.out.println("\n ------------------------------------------------------------- \n");
            // ナレーション <ppt 44>
            System.out.println("神様は再び姿が消えた。\n \nしかし、物語はつづく。");
            System.out.println("Press Enter Key...");
            scan.nextLine();
            System.out.println("\n ------------------------------------------------------------- \n");
    // ----------------------------------------------------------------------------------------------------------------------------
// Scene 3
    // Normalの場合、信長への回答  <ppt 45>
            p.answer44(args);
            System.out.println("Press Enter Key...");
            scan.nextLine();
            System.out.println("\n ------------------------------------------------------------- \n");
    // Normalの場合、Playerへの返事  <ppt 46>
            n.answer11(args);
            System.out.println("Press Enter Key...");
            scan.nextLine();
            System.out.println("\n ------------------------------------------------------------- \n");
    // ----------------------------------------------------------------------------------------------------------------------------
            Thread.sleep(3000);
            // Scene 4
    // ナレーション <ppt 48>
            System.out.println("明智は夜を徹して本能寺に迫ってきた。\n \nそして、暗くなると、森の影に秀吉の姿が現れた。二人は目で何かの合図を交わした。\n \nしばらくの沈黙がカラスの鳴き声で>引き裂かれた。いよいよ、本能寺の変が始まった。");
            System.out.println("Press Enter Key...");
            scan.nextLine();
            System.out.println("\n ------------------------------------------------------------- \n");
            Thread.sleep(1000);
    // 明智への合図  <ppt 49>
            h.speak23(args);
            System.out.println("Press Enter Key...");
            scan.nextLine();
            System.out.println("\n ------------------------------------------------------------- \n");
    // 秀吉への合図  <ppt 50>
            a.answer31(args);
            System.out.println("Press Enter Key...");
            scan.nextLine();
            System.out.println("\n ------------------------------------------------------------- \n");
    // 進撃する
            h.run21(args);
            a.run31(args);
            System.out.println("Press Enter Key...");
            scan.nextLine();
            System.out.println("\n ------------------------------------------------------------- \n");
            Thread.sleep(3000);
     // ナレーション <ppt 51-54>
            NormalHistory.normal(args);
            System.out.println("~END~");
            }else if(choice == 9){
    // Changed Hitory
    // ナレーション <ppt 43>
            System.out.println("神様：そうですか。この未来を選びましたね！\n \nなら、気を付けてください。");
    // ナレーション <ppt 44>
            System.out.println("神様は再び姿が消えた。\n \nしかし、物語はつづく。");
    // ----------------------------------------------------------------------------------------------------------------------------
    // Scene 3
    // Changedの場合、信長への回答  <ppt 56>
            p.answer45(args);
            System.out.println("Press Enter Key...");
            scan.nextLine();
            System.out.println("\n ------------------------------------------------------------- \n");
    //Changedの場合、Playerへの拒否反応  <ppt 57>
            n.reject11(args);
            System.out.println("Press Enter Key...");
            scan.nextLine();
            System.out.println("\n ------------------------------------------------------------- \n");
    // 信長を忠告する  <ppt 58>
            p.speak44(args);
            System.out.println("Press Enter Key...");
            scan.nextLine();
            System.out.println("\n ------------------------------------------------------------- \n");
    // Normalの場合、Playerへの返事  <ppt 59>
            n.answer11(args);
            System.out.println("Press Enter Key...");
            scan.nextLine();
            System.out.println("\n ------------------------------------------------------------- \n");
            Thread.sleep(3000);
    // ----------------------------------------------------------------------------------------------------------------------------
 // Scene 4
    //撤退する  <ppt 61>
    // ナレーション <ppt 44>
            System.out.println("あなたの忠告を受け入れ、\n信長はこっそりと家臣たちと一緒に山の奥へ逃げました");
            n.speak15(args);
            System.out.println("Press Enter Key...");
            scan.nextLine();
            System.out.println("\n ------------------------------------------------------------- \n");
            Thread.sleep(3000);
    // ナレーション <ppt 62-66>
            ChangedHistory.change(args);
            System.out.println("~END~");
            }else{
                System.out.println("正しい選択肢を入力してください（数字のみ）");
                // 強制終了
                Thread.sleep(5000);
                System.out.println("予期せぬエラーが発生しました。ゲームを終了します");
                }
        } catch(InterruptedException e){
            //割り込み例外
        System.out.println("プログラムの割り込みが発生しました");
        e.printStackTrace();
        }
    }
}
