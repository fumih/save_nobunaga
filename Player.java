public class Player extends Character {
	
    // 本能寺の広さに驚いた
	public void speak41(String[] args){
        super.speak(args);
        System.out.println("Player:本能寺って、こんなに広いのか～すごい！");
	}
	
	// 信長との挨拶1
	public void question41(String[] args){
        super.question(args);
        System.out.println("Player:おじいさん、すみません、お寺のスタッフさんですか？");
	}
	
	// 信長との挨拶2
	public void question42(String[] args){
        super.question(args);
        System.out.println("Player:うわ～怖っ！\n \n今本能寺にいるから、もしかしておじいさんは織田信長ですか？");
	}
	
	// 自己紹介セリフ1
    public void speak42(String[] args){
        super.speak(args);
        System.out.println("Player:私？\n私は未来から来た人なんです。");
    }
    
	// 自己紹介セリフ2
    public void speak43(String[] args){
        super.speak(args);
        System.out.println("Player:本当です！信じてください。");
    }
	
	// 信長への返事
    public void answer41(String[] args){
        super.answer(args);
        System.out.println("Player:はい。承りました。");
	}
	
	// 神様の選択へ返事する1
	public void answer42(String[] args){
        super.answer(args);
        System.out.println("Player:それは…");
	}
	
	// 神様の選択へ返事する2
	public void answer43(String[] args){
        super.answer(args);
        System.out.println("Player:そうですね…");
	}
    
	// Normalの場合、信長への回答
	public void answer44(String[] args){
        super.answer(args);
        System.out.println("Player:確かにおっしゃった通りです。\nしかし、私、実は歴史が一番不得意な科目です。毎回の試験もギリギリですし。\nすみません。");
	}
	
	// Changedの場合、信長への回答
	public void answer45(String[] args){
        super.answer(args);
        System.out.println("Player:残念ながら、信長様は今夜ここで殺されます。\n歴史上に「本能寺の変」と呼ばれます");
	}
	
	// 信長を忠告する
    public void speak44(String[] args){
        super.speak(args);
        System.out.println("Player:本当です。このことは未来の歴史の教科書に書いてます。誰でも知っている事実です。\n \n信長様、暗くならないうちに、早く逃げてください。");
    }

}
