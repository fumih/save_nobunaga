public class Akechi extends Character{
    // 秀吉への合図
    public void answer31(String[] args){
        super.answer(args);
    	System.out.println("明智光秀：ええ。\nあの魔王を地獄に送り返しましょう！");
    }
    
    // 進撃する
    public void run31(String[] args){
        super.run(args);
        System.out.println("明智光秀が本能寺に迫ります。");
    }
}