public class Nobunaga extends Character {
    
	 // Playerを取り調べるセリフ1
    public void question11(String[] args){
        super.question(args);
        System.out.println("信長：そこの貴方！何者でござる？何を見ている？");
    }
    
    // Playerを取り調べるセリフ2
    public void question12(String[] args){
        super.question(args);
        System.out.println("信長：何のことだ！名を乗れ！");
    }
    
    // Playerを取り調べるセリフ3
     public void question13(String[] args){
        super.question(args);
        System.out.println("信長：わしの名前を知っているのか！やはり怪しいやつでござる。貴様の名前を言え！");
    }
    	
	// Playerを取り調べるセリフ4
     public void question14(String[] args){
        super.question(args);
        System.out.println("信長：未来からの者？\n \n信じられるか！本当のことを言え！");
    }
    	
    // Playerを取り調べるセリフ5
     public void question15(String[] args){
        super.question(args);
        System.out.println("信長：まあええ。\nちょうどうちは今使用人を探してるから、雇おう。\nついてこい。");
    }
    	
    // 場所用意を命令する
    public void speak11(String[] args){
        super.speak(args);
        System.out.println("信長：午後から本殿で全員集合する！そこを準備してくれ。");
    }
    	
    // 信長の天下構想を語る1
    public void speak12(String[] args){
        super.speak(args);
        System.out.println("信長：いいか。天下というもんは、武を布くべし！\nわし、いや、織田家しかそんなことはできぬ！\n文人などは何も分からぬ。");
    	System.out.println("信長：乱世の中で平和を手に入れるには、\n武力は必要でござる。\nそして、わしは絶対にこの手で天下を取る！");
    }
    
    // 秀吉との対話1
     public void question16(String[] args){
        super.question(args);
        System.out.println("信長：サル、お前は何か違うことを言いたいか？");
    }
    
    // 信長の天下構想を語る2
    public void speak13(String[] args){
        super.speak(args);
        System.out.println("信長：先の事ならわしも考えておるわ！\n天下を取った後、一部に楽市楽座を残し、\n商業の発展、その利益を俺が押さえる。\n \nこの国を安定させたら、その次は世界を取る。\n面白いだろう？");
    }
    
    // 秀吉との対話2
     public void speak14(String[] args){
        super.speak(args);
        System.out.println("信長：それはならぬ！もうええ、ここでええ。");
    }	
    	
    // Playerに本能寺の変を聞く
     public void question17(String[] args){
        super.question(args);
        System.out.println("信長：前、貴様は未来から来たと申したな！\nでは聞こう。\nわし、未来の織田はどうなるのだ？");
    }
    	
    // Normalの場合、Playerへの返事
    // Changedの場合、忠告をもらった後も一緒
    public void answer11(String[] args){
        super.answer(args);
    	System.out.println("信長：…そうでござるか。下がっていい。");
    }
    	
    //Changedの場合、Playerへの拒否反応
    public void reject11(String[] args){
        super.reject(args);
    ｝
    
    	
    //撤退する
    public void speak15(String[] args){
        super.speak(args);
    	System.out.println("信長：蘭丸、尾張まで逃げるぞ");
    }
}