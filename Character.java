public class Character{
    // 名前の宣言
    String name;
    
    // 普通に話すセリフ
    public static void speak(String[] args){
        System.out.println("");
    }
    
    // 普通に質問するセリフ
    public static void question(String[] args){
        System.out.println("");
    }
    
    // 拒否反応のセリフ
    public static void reject(String[] args){
        System.out.println("わしが殺される？\n貴様は二度と口を開くな！言葉を慎め！\nわしが殺されるわけがどこにある?");
    }
    
    // 普通に回答するセリフ
    public static void answer(String[] args){
        System.out.println("");
    }
    
    // 進撃する
    public void run(String[] args){
        System.out.println("");
    }
    
}