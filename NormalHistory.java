public class NormalHistory{
    
    // 本能寺の変が発生したEnding
    
    public static void normal(String[] args){
    	System.out.println("秀吉と明智は手を組んで、\n大勢の軍隊が一気に本能寺を攻めた。");
        System.out.println("本能寺の変をもって、織田時代の幕が終わりました。その後、羽柴秀吉は名前を「豊臣秀吉」に変え、豊臣時代の幕が開きました。");
        System.out.println("一方、歴史の整合性に従って、あなたはある夜にそっと本来の世界に戻りました。");
    }
}