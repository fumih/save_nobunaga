public class ChangedHistory {
    
    // 本能寺の変が発生しないEnding
	
    public static void change(String[] args){
        System.out.println("やがて間一髪のところで明智の襲撃を逃れた信長は、その後、本能寺の変の主犯としての明智を打ち倒し、その後秀吉や徳川らとともに天下を統一した。かねてより宣言していた天下布武を現実のものとしたのだ。");
    	System.out.println("織田信長が没後して、長子の織田信忠が引継ぎして、約100年間の繁栄時代をもたらしした。");
    	System.out.println("しかし、ノブナガの野望は止まらない。\n \n国内の統一が終わった今、ノブナガはオランダ人から贈答された地球儀を眺めていた。");
        System.out.println("また、歴史が変わったため、あなたがそのままで戦国時代に生きてゆきました。");
    }
}