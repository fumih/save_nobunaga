public class Hideyoshi extends Character {
    
    // 秀吉の天下構想を語る1
    public void speak21(String[] args){
        super.speak(args);
        System.out.println("羽柴秀吉：そんなことはございません。\nただし、乱世の平和は長続きできるものではありませぬ。\nその先のことも考えた方がよろしいのではござらぬか？\n \n拙者は、永久に続く平和を保証してこそ、本当の「天下を　取った」ことと存じます。");
    }
    
	 // 秀吉の天下構想を語る2
    public void speak22(String[] args){
        super.speak(args);
        System.out.println("羽柴秀吉：拙者は民を完全に支配することを考えます。\n二度と反乱が起こらないように、天下をとったら、まずは刀狩りを。そして武器を持たせないようにするのです。");
    }
	
	// 明智への合図
	public void speak23(String[] args){
        super.speak(args);
        System.out.println("羽柴秀吉：行くぞ!");
	}
        
    // 進撃する
    public void run21(String[] args){
        super.run(args);
        System.out.println("羽柴秀吉が本能寺に迫ります。");
    }
}